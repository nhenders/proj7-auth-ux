# Brevet time calculator with Ajax and MongoDB

Simple list of controle times from project 4 stored in MongoDB database. Takes input using jQuery, then processes the information using ajax flask. Output is reflect immediately using ajax. This calculator runs on port 5000. 

Added "Submit" and "Display" buttons. When "Submit" is clicked, the control times currently being displayed are entered into a MongoDB database. Before they are added, the database is cleared - it will only hold controls for one brevet at a time. When "Display" is clicked, whatever is presently in the database is displayed on a fresh page with only the results on it.

At least one control must be entered - otherwise, an error message will be shown. Control times are calculated using the logic described here (https://rusa.org/pages/acp-brevet-control-times-calculator). It is assumed that all distances entered are valid control points, so the last one is not >+/-20% from the brevet distance. Per rusa.org, small discrepancies between the final control point and the brevet distance do not affect the open/close times.

The close time for the starting control (0km) is 1 hour after the start time. See paragraph 1 of "oddities" in the link above.

# Project 7: Authentication and UX

Between finals and family problems I did not have enough time to complete this assignment and get everything working. I'm going to openly describe what works and what doesn't so you don't have to dig around too much. Please have mercy on me, Yukhe. 

Part 1: 

* POST /api/register works as expected, returning a JSON object with a Location header
* GET /api/token works as expected, returning a JSON object with a token field
* GET /RESOURCE-YOU-CREATED-IN-PROJECT-6 is implemented but not usable. The API functions attempt to verify the token before returning any data, but I was unable to actually pass the token into these calls.
* All API calls are working on 0.0.0.0:5001

Part 2:

* 0.0.0.0:5002/ holds the Login/Register/Logout UI
* All buttons work and relevant forms contain CSRF protection
* Form submissions do not always redirect somewhere meaningful but are indeed submitted

# Project 6: RESTful API

Port 5002 can be used for individual calls to the API. You can make calls in the following format, and expect the following behavior:

* "http://host:port/listAll" should return all open and close times in the database
* "http://host:port/listOpenOnly" should return open times only
* "http://host:port/listCloseOnly" should return close times only
* "http://host:port/listAll/csv" should return all open and close times in CSV format
* "http://host:port/listOpenOnly/csv" should return open times only in CSV format
* "http://host:port/listCloseOnly/csv" should return close times only in CSV format
* "http://host:port/listAll/json" should return all open and close times in JSON format
* "http://host:port/listOpenOnly/json" should return open times only in JSON format
* "http://host:port/listCloseOnly/json" should return close times only in JSON format
* "http://host:port/listOpenOnly/csv?top=3" should return top 3 open times only in CSV format
* "http://host:port/listOpenOnly/json?top=5" should return top 5 open times only in JSON format
* "http://host:port/listCloseOnly/csv?top=6" should return top 5 close times only in CSV format
* "http://host:port/listCloseOnly/json?top=4" should return top 4 close times only in JSON format

Note that CSV output is in the form of a CSV string, so lines are delimited with \n. 

## Button Test Cases

**Submit**

* No control times entered -> Error message shown beneath button
* Control times are entered -> Add to Mongo database, no output shown
* Pressed twice -> "Writes over" previously stored data with current data

**Display**

* No control times entered, first use of "Display" *ever* -> Presents empty table with labels
* All other states -> Presents formatted table with whatever relevant information is currently being held in the database

## Contact

Nick Henderson

nhenders@uoregon.edu / nick@nihenderson.com

