#Laptop Service

from flask import Flask, render_template, request, make_response, request, redirect, url_for, session, jsonify
from flask_restful import Resource, Api
from pymongo import MongoClient
import os
from flask_login import (LoginManager, current_user, login_required,
        login_user, logout_user, UserMixin, 
        confirm_login, fresh_login_required)
from itsdangerous import (TimedJSONWebSignatureSerializer
                            as Serializer, BadSignature, SignatureExpired)
from random import randint
from wtforms import Form, PasswordField, BooleanField, StringField, SubmitField
from wtforms.validators import DataRequired
from passlib.apps import custom_app_context

# Instantiate the app
app = Flask(__name__)
app.config['SECRET_KEY'] = str(randint(0,99999))
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
users = db.userdb
data = []

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = ('/api/login')

header = {'Content-Type': 'text/html'}

class ListAll(Resource):
    def get(self, rep="json"):
        if not verify_auth_token(request.args.get('token')):
            return 'Token error'

        rep = "json" if rep is None else rep
        items = db.tododb.find_one()

        if rep == "csv":
            ret = "open,close\n"
            for control in items['controls']:
                data = list(items['controls'][control].values())
                ret += data[2] + "," + data[3] + "\n"  # both open and close

        else:
            ret = {"open": [], "close": []}
            for control in items['controls']:
                data = list(items['controls'][control].values())
                ret["open"].append(data[2])
                ret["close"].append(data[3])


        return ret


class ListOpenOnly(Resource):
    def get(self, rep="json"):
        if not verify_auth_token(request.args.get('token')):
            return 'Token error'

        rep = "json" if rep is None else rep
        items = db.tododb.find_one()

        if rep == "csv":
            ret = "open,close\n"
            for control in items['controls']:
                data = list(items['controls'][control].values())
                ret += data[2] + "," + data[3] + "\n"  # both open and close

        else:
            ret = {"open": [], "close": []}
            for control in items['controls']:
                data = list(items['controls'][control].values())
                ret["open"].append(data[2])
                ret["close"].append(data[3])


        return ret


class ListOpenOnly(Resource):
    def get(self, rep="json"):
        if not verify_auth_token(request.args.get('token')):
            return 'Token error'

        rep = "json" if rep is None else rep
        items = db.tododb.find_one()

        if rep == "csv":
            ret = "open,close\n"
            for control in items['controls']:
                data = list(items['controls'][control].values())
                ret += data[2] + "," + data[3] + "\n"  # both open and close

        else:
            ret = {"open": [], "close": []}
            for control in items['controls']:
                data = list(items['controls'][control].values())
                ret["open"].append(data[2])
                ret["close"].append(data[3])


        return ret


class ListOpenOnly(Resource):
    def get(self, rep="json"): 
        rep = "json" if rep is None else rep
        items = db.tododb.find_one()
        ret = []
        i = 0;

        amount = request.args.get('top')
        amount = 20 if amount is None else int(amount)

        if rep == "csv":
            ret = "open\n"
            for control in items['controls']:
                if i >= amount:
                    break
                ret += list(items['controls'][control].values())[2] + "\n"
                i+=1

        else:
            ret = {"open": []}
            for control in items['controls']:
                if i >= amount: break
                data = list(items['controls'][control].values())
                ret["open"].append(data[2])
                i+=1

        return ret


class ListCloseOnly(Resource):
    def get(self, rep="json"):
        rep = "json" if rep is None else rep
        items = db.tododb.find_one()
        ret = []
        i = 0;

        amount = request.args.get('top')
        amount = 20 if amount is None else int(amount)

        if rep == "csv":
            ret = "close\n"
            for control in items['controls']:
                if i >= amount: break
                ret += list(items['controls'][control].values())[3] + "\n"
                i+=1

        else:
            ret = {"close": []}
            for control in items['controls']:
                if i >= amount: break
                data = list(items['controls'][control].values())
                ret["close"].append(data[3])
                i+=1

        return ret

# LOGIN FUNCTIONS

class LoginForm(Form):
    username = StringField('Username', validators=[DataRequired(message=u'Username is required')])
    password = StringField('Password', validators=[DataRequired(message=u'Password is required')])
    remember_me = BooleanField('Remember Me')

class RegisterForm(Form):
    username = StringField('Username', validators=[DataRequired(message=u'Username is required')])
    password = StringField('Password', validators=[DataRequired(message=u'Password is required')])


class User(UserMixin):
    def __init__(self, user_id):
        self.id = str(user_id)


@app.route("/api/register", methods=["GET", "POST"])
def register():

    form = RegisterForm(request.form)
    username = form.username.data
    password = form.password.data

    if form.validate():
        item = db.users.find_one({"username":username})
    
        print("ITEM: " + str(item))
        if username is None or password is None:
            return 'Username and password is required', 400

        hashed = custom_app_context.encrypt(password)
        _id = randint(0,99999)
    
        user = {"_id": _id, "username": username, "password": hashed}
        db.users.insert_one(user)

        ret = {"_id": _id, "username": username}

        return (jsonify(ret), 201, {'Location': url_for('get_user', _id=_id)})

    return render_template('register.html', form=form)

def generate_auth_token(_id, expiration=600):
    s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)

    return {'token': s.dumps({'_id': _id}), 'duration': expiration}

def verify_auth_token(token):
    s = Serializer(app.config['SECRET_KEY'])
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None
    except BadSignature:
        return None
    return db.users.find_one({"_id": _id})

@app.route('/api/users/<int:_id>')
def get_user(_id):
    user = db.users.find_one({"_id": _id})

    return jsonify({'username': user['username']})

@app.route("/api/login", methods=["GET", "POST"])
def login():
    form = LoginForm(request.form)

    if form.validate():
        username = form.username.data
        password = form.password.data
        remember_me = form.remember_me.data

        user = db.users.find_one({"username":username})

        if user is None:
            return redirect(url_for("register"))

        hashed = user['password']
        if custom_app_context.verify(password, hashed):
            active = user['_id']
            user['_id'] = active
            user = User(active)
            login_user(user, remember=remember_me)
            
            token = generate_auth_token(active)['token'].decode("utf-8")
            return render_template('index.html', token=token) #FIXME probably want to change this
        else: return render_template('register.html', form=form)
    
    else:
        return render_template('login.html', form=form)

@login_manager.user_loader
def load_user(user_id):
    if user_id is None: return None
    return None # render_template("index.html")

@app.route("/api/token", methods=['GET'])
@login_required
def token():
    _id = session.get('_id')
    token = generate_auth_token(_id, 600)
    
    return flask.jsonify({'token': token, 'duration': 600})

@app.route("/api/logout")
@login_required
def logout():
    logout_user()
    return redicrect(url_for("index"))

@app.route("/")
def index():
    return render_template("index.html")


# Create routes
# Another way, without decorators
api.add_resource(ListAll, '/listAll/', '/listAll/<rep>')
api.add_resource(ListOpenOnly, '/listOpenOnly/', '/listOpenOnly/<rep>')
api.add_resource(ListCloseOnly, '/listCloseOnly/', '/listCloseOnly/<rep>')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
